###  
#### MiniX3
  [Click here for the throbber](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX3/)

  [Click here for the code](https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX3/sketch.js)

#### GIF of my throbber
![](Loading_tree.gif)


#### Description of my throbber design.

#####  What do you want to explore and/or express?
I wanted to express how quickly time passes by and also show how much time you are using on just waiting on something. So I started by thinking of things where you can see changes over a longer period of time. This is where I thought of the season changes. You can feel and see the different seasons. When it is winter it is cold and dark outside, the hours of sun light is minimal in winter which makes the days feel short. As we approach spring the days are a bit longer because the sun is out a longer period of time and the freezing cold weather is beginning to be a bit more warmer. The summer is warm and the sun is up for almost the entire day. Then autumn approaches and it begins to be colder and the hours of sun light shorter. and then the cycle starts all over again. But it is not only a cycle of seasons that will change, it would also be a new year, making a thought of displaying the season changes a lot more scary, because not only will the season changes quickly, but in some way you will also look at it and think wow the years goes by fast. So next thought was how to display the changes in season?

One way you can also see how those seasons changes, besides the weather and time of sun light, is by the trees. When it is winter there is no leaves on the branches, maybe a bit of snow instead. When it is spring the leaves are beginning to come. When it is summer all the leaves have sprouted from the branches. As autumn approaches the leaves are beginning to fall off. And then we have winter again. This cycle is what my throbber displays - the changes in season through trees. With this a person looking at the throbber will see how quickly the seasons changes but it also incorporates a sense of how fast calendar system goes by - thus how a new year ends and a new one starts all the time.

##### What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?
All technically I started by making the four trees. Afterwards I made functions for each of these trees. The reason I did this was for making the code more organized and making the next step less complicated. The next step for me was to make the trees shift between the different trees I have made – using time-related syntaxes.
For this I used the if statement/conditional structure. To do this I started by create and name a variable ‘trees’ = 0 with help of let. The variable I then used to say that every time the frame draw() is called ‘trees’ is incremented with one. In the if statement I used the remainder; ’%’, to create a condition that says (trees % 9 == 1). Every time the remainder is 1 this statement will then be true and the code between the following curly braces is then run, which in this case is my function winter_tree(). This I then did for each of my tree functions, but with other condition, making the shift between the trees. I do not know if this was the best way to make the wanted shift between trees, but this was worked for me and the best way I could think of. The trees shifted very quickly so I used frameRate() to make it more slow. To center the trees to the point I wanted to I used translate. To finish of the work I added the text Loading and used the for() loop to create the three dots after the text.  


##### What does a throbber communicates, and/or hides? How might we characterize this icon differently?
A throbber that I have encounter in digital culture and one that I have encountered a lot is the bars that rotates in a circle. I see it a lot of places by example when using Sportify or Instagram. The throbber communicates that the program I am using is in the process of performing the requested action. It is giving feedback from the action I have just made that the program now knows what I want and it will try to meet the desired request, but in 5 seconds or more. The action is set in motion, but you will have to wait. That is what the throbber hides, it hides how long you acutely have to wait, maybe it is just 5 seconds, but it could also be 1 minute. But it is still showing a response on the users action, which I think is better than nothing, because then the user would not know whether what you just did worked. So what the bar which runs in a circle might need, is to somehow show that this not necessarily just 5 seconds you have to wait it might be more.


###### References
* Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96

* Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)
