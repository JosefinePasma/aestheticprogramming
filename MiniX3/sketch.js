let trees = 0
let t = 0

function setup() {
 createCanvas(windowWidth, windowHeight);
 frameRate(2.5);
}

function draw() {
  background(164, 238, 243);
  trees += 1
  translate(windowWidth/6, windowHeight/10)
  if(trees % 9 == 1){
    winter_tree();
  }
  if (trees % 9 == 3){
    spring_tree();
  }
  if (trees % 9 == 5){
    summer_tree();
  }
  if (trees % 9 == 7){
    autumn_tree();
  }
  noStroke();
  fill(255);
  textSize(50);
  text("Loading",260,590,340,150);
  for(let t = 0; t < 60; t++){
    ellipse(450+t, 625, 9);
    t += 20;
  }
}

function winter_tree () {
//snow
  noStroke();
  fill(255);
  ellipse(380, 545, 190, 30);
//tree trunk
  fill(90, 65, 55);
  quad(365, 350, 395, 350, 410, 540, 350, 540);
  ellipse(380, 359, 30,80);
//tree branches
  stroke(85, 60, 50)
  strokeWeight(8);
  line(380, 350, 440, 330);//right top
  line(324, 316, 380, 332);//left top
  line(320, 348, 370, 365);//left second top
  line(290, 380, 376, 400);//left middle
  line(390, 385, 480, 365);//right second top
  line(385, 415, 440, 410);//right second bottom
  line(260, 410, 370, 430);//left second bottom
  line(400, 450, 510, 430);//right bottom
  line(315, 460, 372, 465);//left bottom
//more snow
  noStroke();
  fill(255);
  ellipse(415, 370, 43, 12);
  ellipse(347, 383, 30, 10);
  ellipse(272, 405, 34, 7);
  ellipse(363, 316, 75, 17);
  ellipse(330, 415, 60,10);
  ellipse(435, 438, 60,10);
  ellipse(335, 457, 40, 7);
  ellipse(413, 405, 30, 7);
//more tree branches
  stroke(80, 55, 45);
  strokeWeight(4);
  line(270, 395, 290, 415);
  line(305, 370, 325, 400);
  line(305, 370, 325, 400);
  line(345, 430, 345, 446);
  line(465, 420, 490, 450);
  line(475, 350, 450, 370);
  line(445, 395, 420, 410);
  line(445, 395, 420, 410);
  line(422, 448, 445, 460);
  line(425, 430, 406, 446);
  line(425, 316, 404, 339);
}

function spring_tree () {
//grass
  noStroke();
  fill(167, 237, 137);
  ellipse(380, 545, 190, 30);
//tree trunk
  fill(90, 65, 55);
  quad(365, 350, 395, 350, 410, 540, 350, 540);
  ellipse(380, 359, 30,80);
//tree branches
  stroke(85, 60, 50)
  strokeWeight(8);
  line(380, 350, 440, 330);//right top
  line(324, 316, 380, 332);//left top
  line(320, 348, 370, 365);//left second top
  line(290, 380, 376, 400);//left middle
  line(390, 385, 480, 365);//right second top
  line(385, 415, 440, 410);//right second bottom
  line(260, 410, 370, 430);//left second bottom
  line(400, 450, 510, 430);//right bottom
  line(315, 460, 372, 465);//left bottom
//leaves
  strokeWeight(3);
  stroke(167, 237, 137);
  fill(173, 242, 124);
  ellipse(480, 360, 80, 20);
  ellipse(354, 350, 60,10)
  ellipse(317,453, 60, 10)
  stroke(225, 55, 36);
  fill(230, 60, 41);
  ellipse(404,339, 58, 15);
  ellipse(293,372, 95, 25)
  ellipse(430, 405, 50, 10)
  ellipse(350, 418, 50, 10)
  stroke(250, 250, 107);
  fill(255, 255, 112);
  ellipse(310, 335, 100, 30);
  ellipse(450,330, 50, 10)
  ellipse(500, 430, 60, 10)
  ellipse(300, 418, 40, 10)
  stroke(250, 156, 56);
  fill(255, 161, 61);
  ellipse(380, 300, 170, 65);//the top leaves
  ellipse(420, 373, 60, 15)
  ellipse(260, 405, 50, 10)
  ellipse(425, 440, 70, 10)
}

function summer_tree () {
//grass
  noStroke();
  fill(113, 214, 73);
  ellipse(380, 545, 190, 30);
//tree trunk
  fill(90, 65, 55);
  quad(365, 350, 395, 350, 410, 540, 350, 540);
  ellipse(380, 359, 30,80);
//tree branches
  stroke(85, 60, 50)
  strokeWeight(8);
  line(380, 350, 440, 330);//right top
  line(324, 316, 380, 332);//left top
  line(320, 348, 370, 365);//left second top
  line(290, 380, 376, 400);//left middle
  line(390, 385, 480, 365);//right second top
  line(385, 415, 440, 410);//right second bottom
  line(260, 410, 370, 430);//left second bottom
  line(400, 450, 510, 430);//right bottom
  line(315, 460, 372, 465);//left bottom
//leaves
  strokeWeight(3);
  stroke(0,170,0);
  fill(0,177,0);
  ellipse(317, 442, 90, 40);//left bottom
  ellipse(465, 420, 140, 40);//right bottom
  ellipse(290, 404, 140, 40);//left second bottom
  ellipse(450, 388, 190, 40);//right second top
  ellipse(350, 375, 90, 40);//left middle
  ellipse(280, 370, 110, 30);//left middle
  ellipse(355, 350, 90, 20);//left the second top
  ellipse(300, 334, 130, 45);//left the second top
  ellipse(435, 350, 160, 45);//right second top
  ellipse(380, 300, 210, 80);//the top leaves
}

function autumn_tree () {
//grass
  noStroke();
  fill(167, 237, 137);
  ellipse(380, 545, 190, 30);
//tree trunk
  fill(90, 65, 55);
  quad(365, 350, 395, 350, 410, 540, 350, 540);
  ellipse(380, 359, 30,80);
//tree branches
  stroke(85, 60, 50)
  strokeWeight(8);
  line(380, 350, 440, 330);//right top
  line(324, 316, 380, 332);//left top
  line(320, 348, 370, 365);//left second top
  line(290, 380, 376, 400);//left middle
  line(390, 385, 480, 365);//right second top
  line(385, 415, 440, 410);//right second bottom
  line(260, 410, 370, 430);//left second bottom
  line(400, 450, 510, 430);//right bottom
  line(315, 460, 372, 465);//left bottom
//more tree branches
  stroke(80, 55, 45);
  strokeWeight(4);
  line(270, 395, 290, 415);
  line(305, 370, 325, 400);
  line(305, 370, 325, 400);
  line(465, 420, 490, 450);
  line(475, 350, 450, 370);
  line(445, 397, 420, 410);
  line(445, 395, 420, 410);
  line(422, 448, 445, 460);
  line(425, 430, 406, 446);
  line(425, 316, 404, 339);
  line(345, 430, 345, 448);
//leaves
  strokeWeight(3);
  stroke(225, 55, 36);
  fill(230, 60, 41);
  ellipse(380, 310, 78,30);
  ellipse(299, 360, 13, 25)
  ellipse(418, 365, 10, 20);
  ellipse(330, 406, 10, 15);
  ellipse(430, 428, 8, 15);
  ellipse(488, 455, 10, 18);
  ellipse(335, 475, 10, 18);
  ellipse(450, 465, 8, 15);
  ellipse(300, 530, 30, 10)
  ellipse(340, 532, 30, 10)
  ellipse(420, 535, 30, 10)
  stroke(250, 250, 107);
  fill(255, 255, 112);
  ellipse(430, 320, 13, 24)
  ellipse(325, 362, 10, 20);
  ellipse(257, 395, 35, 10);
  ellipse(320, 535, 30, 10)
  ellipse(375, 538, 30, 10)
  ellipse(455, 535, 30, 10)
  ellipse(345, 450, 5, 10);
  stroke(250, 156, 56);
  fill(255, 161, 61);
  ellipse(350, 345, 10, 20);
  ellipse(485, 350, 35, 10);
  ellipse(448, 397, 10, 18);
  ellipse(295, 430, 10, 20);
  ellipse(355, 542, 30, 10)
  ellipse(395, 542, 30, 10)
  stroke(225, 55, 36);
  fill(230, 60, 41);
  ellipse(440, 539, 30, 10)
  ellipse(370, 545, 30, 10)
}
