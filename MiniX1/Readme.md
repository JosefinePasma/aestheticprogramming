### My first experience
##### MiniX1
  [Click to see a very cool picture](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX1/)

  [Click to see a very cool coding something](https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX1/sketch.js)


##### What have you produced?
So, the RunMe started with me not knowing what to do, so I just played a bit around with colors and shapes. During this, I got a handle on how the syntax of Color(R, G, B) works. In addition, I learned and understood new shape codes and their syntax. With this, I chose that I want to make a car. I chose that I want to make a car because it will consist of several to make several different shapes and colors as a car consists of several different parts. A hard enough challenge for me. I thought, but actually, I got the hang of it pretty quickly. Therefore I wanted to make more, maybe something with movement. To get inspiration for things I could add I looked at the [Aesthetic-programming.net showcase page](https://aesthetic-programming.net/pages/showcase.html) here I found [Jane Clausen’s first MiniX](https://gitlab.com/JaneCl/ap-2020/-/blob/master/public/MiniEx1/READMEMiniX1.md ) which consists of a yellow background with different color circles there have movement. This inspired me to add some “moving stones” to my RunMe using the same form as Clausen. After adding them and actually making a movement work (after a lot of time figuring out how the form worked), I decided to add one more movement; blinking lights. Here I used a form that I discovered on [p5.js reference-page](https://editor.p5js.org/doubleshow/sketches/BJdU6tFSM), which is a form where [if/else](https://p5js.org/reference/#/p5/if-else) is used. After this I decided that the RunMe was done and I now have a car, a moon, moving stones (because why not moving?), and blinking lights.  

![](Car_on_the_rocks.png)

##### How would you describe your first independent coding experience?
My first independent coding experience I must say was very exciting and rewarding. I spend a lot of time on the project, not because it was necessary, but because I wanted to. When I first started I couldn't stop. It was fun and interesting to learn how the syntax of different shapes worked and how making movement worked. Playing around with numbers and different codes helps to get a bigger understanding of the syntax, which is very giving to learn what you can make and to read the code. Besides playing around it has also been very giving for me to look at others code and looking on [p5.js reference-page](https://p5js.org/reference/). Here I can see how other people make things work and then modify it to my work  starting out with copy-paste, but with an actual understanding of what they have done.

All-in-all a very successful  first independent coding experience.

##### How is the coding process different from, or similar to, reading and writing text?
Well, first of all, coding is very different from reading and writing when it comes to the use of parentheses, semicolons, curly brackets, even comas and etc. The use of these is a lot bigger in coding than in reading and writing. This is not the only reason coding is different from reading and writing, another one is that we have to give the exact orders - in the exact order – and no mistakes are welcome. If the commands are not entirely correct or specific enough you'll encounter a problem or end with an entirely blank page. The same thing will not happen in writing or reading. As a last reason for why there is a difference, I would say that when you read or write you imagine what is said whereas in coding an actual picture is shown as an output of the input.

##### What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?
Code means translating human language to language a machine understands; machine language. Programming means the process of building and creating instructions for the machine to follow and perform. For me code and programming means that being able to do this possibilites will be created.







###### References
>Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48

>Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (on blackbord under the Literature folder)
