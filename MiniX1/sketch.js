//let and var creates and names a new variable
  //light_blink_counter helps making the blinking light
let light_blink_counter = 0


function setup() {
  //createCanvas is used to quite literally create a canvas
  createCanvas(600,600,);
  //the diameter of the "stones"
  diameter = 80;
}

function draw() {
//with this I'm able to draw on the canvas
  //first making the background color with RGB (making colors with red, green and blue)
  background(30,0,95);
  //then placing the things I have already drawn, which are functions I have made
  draw_moon()
  draw_stones()
  draw_car()
  draw_lights ()
}


function draw_moon() {
//vertex and beziervertex is here used with beginshape and endshape functions
//within beginshape and endshape a the first anchor point must be called
//after the moon can be shaped with beziervertex which makes a curve between the position of two control points
  fill(255);
  beginShape();
  vertex(440,60);
  bezierVertex(490,40,490,115,440,115);
  bezierVertex(460,110,460,55,440,60);
  endShape();
}

function draw_car() {
//first starting making the outline of the car black with stroke and using 0 which = black
//then determing the color for the shapes I want to use, for each part
//and lastley choose where the shapes should be placed (x, y) and how they should be shaped (h,w,rounded_corners)
  stroke(0)
  fill(210,0,140);
  rect(107,285,340,147,90);
  fill(0,0,255);
  rect(124,299,309,130,80);
  fill(220,0,0);
  ellipse(62,399,26);
  fill(210,200,0);
  ellipse(508,399,26);
  fill(210,0,140);
  rect(60,363,450,70,10);
  fill(230,100,37);
  ellipse(130,453,69);
  fill(230,100,37);
  ellipse(400,453,69);
  line(278,412,278,300);
  line(60,412,509,412);
  fill(0);
  ellipse(130,453,25);
  fill(0);
  ellipse(400,453,25);
  fill(210,0,140);
  rect(293,372,40,10,30);
  fill(210,0,140);
  rect(142,372,40,10,30);
  line(127,250,190,285);
  line(124,364,130,412);
  line(433,364,427,412);
  line(407,348,432,363);
  fill(0);
  ellipse(406,346,9,24);
}

function draw_stones () {
  fill(100);
  ellipse(330,483,100,10);
  fill(75);
  ellipse(90,490,140,10);
  fill(100);
  ellipse(175,495,100,10);
  fill(75);
  ellipse(455,490,120,10);
  fill(150);
  ellipse(360,492,120,10);
  fill(60);
  ellipse(270,492,140,10);
  fill(125);
  ellipse(223,483,120,10);
}

function draw_lights () {
//to give the "lights" a blinking effect this function is used
//the first line in the function means that each time draw() is called light_blink_counter is incremented with one
//the remainder(%) is what doesn't fit into the divisor.
  //ex: if I divide 25 with 10 it is 2 and the remainder is 5,
  //meaning that the if function will be true and have a blue color for that time draw() is called
//meaning that every 6 time if will be true
  light_blink_counter += 1;
  if (light_blink_counter % 10 <= 5){
    fill(30,0,95);
    triangle(527,399,595,381,595,417);
  } else {
    fill(210,200,0);
    triangle(527,399,595,381,595,417);
  }

  if (light_blink_counter % 10 <= 5){
    fill(30,0,95);
    triangle(40,399,5,390,5,408);
  } else {
    fill(225,0,0);
    triangle(42,399,5,390,5,408);
  }
}
