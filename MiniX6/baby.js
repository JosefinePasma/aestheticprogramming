class Baby {
  constructor() {
    this.x= random(20,width-20)
    this.y = -20;
    this.size = floor(random(40,100));
    this.speed = 3;
  }
  move (){
    this.y += this.speed;
  }

  show() {
    push();
    image(baby1, this.x-10, this.y, this.size, this.size);
    image(baby3, this.x+10, this.y, this.size, this.size);
    image(baby2, this.x, this.y, this.size, this.size);
    pop();
  }
}
