### OBJECT ABSTRACTION
#### MiniX6
The game only works in Google Chrome apparently.
![](THEGAME.png)
  [Click here for the program](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX6/) and   [click here for the code.](https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX6/sketch.js)

#### Description of the program
For this MiniX we had to create a game, I found this difficult and had no idea how to get started. To get started I therefore gathered some inspiration from Winnie's PacMan game. I used inspiration from this to make my own game - a Babyshower. The Babyshower game is a game where you have to catch the babies. You control the persons using your mouse and depending on where the babies will fall move the mouse in order to catch the falling babies. If you do not catch the babies, they will die. The babies will fall in a faster and faster speed. You lose as soon as you do not catch the babies.

For this program it was first when I had finished that I realized that we have to use a 'class-based object-oriented approach' to design our game components - which is stupid when it is the theme of the week. I tried to implement it afterwards - but it did not work as successfully as hoped.  

Now I have implement a 'class-based object-oriented approach' to my work by a Class called Baby, because it is the babies there is my objects, here I decided the size and movements for the objects. Now the program allthough does not have the wished style. It does this wierd thing that when the mouse is at the place where you have to catch the babies, you do catch the babies but first after a while, but as the game goes on the distance of when you catch the babies goes up (if that makes sense) - and I do not jet now how I managed to do that. 

To control the way the game is played, I have used different functions. These include function mousePressed, conditional statements and so on. Besides this I also used a lot of images.

#### Characteristics of oject-oriented programming and wider implications of abstraction as well as the cultural context 
Some of the key characteristics of object-oriented programming is properties and behaviors. It comes from one of the key concepts of object-oriented programming – abstraction, here Soon and Cox explains; “The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model.”(2020). What is useful by this is that you can customize certain elements of the code in a much more manageable way, than with just using functions and logic. Creating classes and what you put in these classes applies throughout the entire code, which helps to decrease the complexity because you just refer to the class and then that class is packed with all you need to make them work in your own program. What is also characteristic about object-oriented programming is that "In the same token, we can “reuse” the same properties and behavior to create another “object instance” with the corresponding data values". What is meant here is that all objects have same method to be described - but have slightly different outcome, just like we as persons can be described with the same method, but the outcome will be different. The Description of us as person will though be very abstract because only certain detail can be highlighted and therefore there is a selective of what is being described. We as persons are just not that non complex, but objects are not;“objects are designed with certain assumptions, biases, and worldviews”(Soon & Cox, 2020). This we must acknowledge in order to create or redefine objects in a way that represents us all.


###### References
* Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

* Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on BrightSpace\Literature)
