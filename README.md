## Hello & welcome
This is my aesthetic programming folder where you can find all my projects in the AP course of 2022. Enjoy exploring! Down below are links (& some gifs) to all the programs' RunMe, for super-easy access.

[MiniX1 - Getting Started](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX1/)
![](MiniX1/Car_on_the_rocks.png)

[MiniX2 - Variable Geometry](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX2/)
![](MiniX2/OGRES_ARE_LIKE_ONIONS.png)

[MiniX3 - Infinite Loop](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX3/)

![](MiniX3/Loading_tree.gif)

[MiniX4 - Data Capture](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX4/)
![](MiniX4/Surveillance.png)

[MiniX5 - Auto-Generator](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX5/)
![](MiniX5/pixelSomething.png)

[MiniX6 - Object Abstraction](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX6/)
![](MiniX6/THEGAME.png)

[MiniX7 - Revisit the past](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX7/)
![](MiniX7/BabyGame2.0.png)

[MiniX8 - Vocable Code](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX8/)
![](MiniX8/Ukraine.gif)

[Final Project](https://josefinepasma.gitlab.io/aestheticprogramming/finalProject/)
![](finalProject/Finish.png)
