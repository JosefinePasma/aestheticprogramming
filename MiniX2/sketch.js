function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(10);
  background(90, 65, 55);
  draw_shrek_normal_face();
  normal=createButton("Normal");
  normal.position(77, 145);
  normal.mousePressed(draw_shrek_normal_face);
  happy=createButton("Happy");
  happy.position(79, 180);
  happy.mousePressed(draw_shrek_happy);
  quirky=createButton("Quirky");
  quirky.position(79, 215);
  quirky.mousePressed(draw_shrek_quirky);
  angry=createButton("Angry");
  angry.position(81, 250);
  angry.mousePressed(draw_shrek_angry);
}

function draw () {
  let w="Click on the buttons to express your Shrek emotion:"
  stroke(70, 45, 35);
  textSize(25);
  fill(random(90, 100),random(65, 75),random(55, 65));
  text(w,50,50,340,150);
  strokeWeight(3);
  stroke(70, 45, 35)
  fill(133, 220, 82)
  ellipse(106, 156, 90, 30);
  ellipse(106, 191, 90, 30);
  ellipse(106, 226, 90, 30);
  ellipse(106, 261, 90, 30);
}

//all the different faces is what are coming up here:
function draw_shrek_normal_face () {
  strokeWeight(3);
  angleMode(DEGREES);

//the shape of face
  fill(153, 240, 102);
  noStroke();
  ellipse(500, 440, 400, 500);
  triangle(735, 545, 660, 290, 650, 720);
  triangle(272, 549, 340, 290, 350, 720);
  ellipse(505, 592, 480, 349);

//the things on his forehead
  stroke(156, 243, 105);
  fill(150, 230, 100);
  ellipse(400, 280, 40, 30);
  ellipse(450, 240, 30, 20);
  ellipse(500, 260, 40, 30);
  ellipse(590, 260, 40, 30);
  ellipse(565, 225, 30, 20);

//ears
  noStroke();
  fill(153, 240, 102,);
  quad(198, 292, 183, 343, 304, 400, 306, 381);//left ear
  ellipse(193, 319, 40, 57);//left ear
  quad(809, 292, 824, 343, 696, 400, 694, 381);//right ear
  ellipse(816, 319, 40, 57);//right ear
  fill(160, 250, 110);
  triangle(194, 298, 183, 332, 215, 328);//left ear
  triangle(812, 298, 823, 332, 791, 328);//right ear
  fill(140, 220, 90,);
  triangle(194, 300, 183, 330, 209, 326);//left ear
  triangle(812, 300, 823, 330, 797, 326);//right ear

//eyes:
  noStroke();
  fill(150, 230, 100);
  ellipse(410, 400, 100, 55);//left outer cicle
  ellipse(590, 400, 100, 55);//right outer cicle
  stroke(153, 210, 102);
  fill(250);
  ellipse(410, 400, 60, 30);//left eyeball
  ellipse(590, 400, 60, 30);//right eyeball
  noStroke();
  fill(80, 55, 45);
  ellipse(410, 400, 18, 24);//left iris
  ellipse(590, 400, 18, 24);//right iris
  fill(0);
  ellipse(410, 400, 9);//left pupil
  ellipse(590, 400, 9); //right pupil
  stroke(148, 238, 98);
  fill(155, 245, 110);
  arc(410, 390, 70, 20, 178, 2, OPEN);//left upper eyelid
  arc(410, 407, 70, 20, 2, 178, OPEN);//left eyelid
  arc(590, 390, 70, 20, 178, 2, OPEN);//right upper eyelid
  arc(590, 407, 70, 20, 2, 178, OPEN);//eyelid

//mouth:
  noStroke();
  fill(180, 146, 104);
  arc(500, 588, 325, 75, 0, 180, OPEN)//underlip
  fill(153, 240, 102);
  ellipse(500, 593, 110, 23)//helping hiding part of underlip
  stroke(158, 245, 107);
  fill(149, 236, 98);
  ellipse(500, 567, 43, 75)//philtrum
  noStroke();
  fill(180, 146, 104);
  arc(390, 607, 222, 45, 200, 0, OPEN);//left upperlip
  arc(610, 607, 222, 45, 180, -20, OPEN);//right upperlip
  stroke(170, 136, 94);
  noFill();
  beginShape(); //line in the mouth
  curveVertex(338, 589);
  curveVertex(338, 589);
  curveVertex(470, 606);
  curveVertex(530, 606);
  curveVertex(662, 589);
  curveVertex(662, 589);
  endShape();
  stroke(150, 230, 100);
  curve(241,460,328,575,426,494,751,600); //left smilewrinkle
  curve(795,460,675,575,590,494,330,600); //right smilewrinkle

//nose:
  noStroke();
  fill(150, 230, 100);
  ellipse(500, 374, 47, 20);//the form of nose
  ellipse(500, 500, 188, 77);//the form of nose
  quad(478, 370, 522, 370, 595, 500, 405, 500);//the form of nose
  fill(100, 190, 90);
  arc(552, 513, 40, 33, 0, 140, OPEN);//right nostril
  arc(450, 513, 40, 33, 10, 200, OPEN); //left nostril
  stroke(140, 220, 90);
  fill(160, 240, 110);
  ellipse(500, 493, 169, 70); //nosetip
  noStroke();
  ellipse(500, 487, 164, 60); //nosetip
  fill(180, 260, 130);
  ellipse(515, 423, 15, 50);//shadow noseline
  ellipse(530, 505, 40, 14)//shadow nosetip

//eyebrows:
  fill(70, 45, 35);
  angleMode(DEGREES);
  arc(400, 370, 160, 50, 180, 0, OPEN);//left eyebrow
  arc(600, 370, 160, 50, 180, 0, OPEN);//right eyebrow
}

function draw_shrek_angry () {
  strokeWeight(3);
  angleMode(DEGREES);

//the shape of face
  fill(153, 240, 102);
  noStroke();
  ellipse(500, 440, 400, 500);
  triangle(735, 545, 660, 290, 650, 720);
  triangle(272, 549, 340, 290, 350, 720);
  ellipse(505, 592, 480, 349);

//the things on his forehead
  stroke(156, 243, 105);
  fill(150, 230, 100);
  ellipse(400, 280, 40, 30);
  ellipse(450, 240, 30, 20);
  ellipse(500, 260, 40, 30);
  ellipse(590, 260, 40, 30);
  ellipse(565, 225, 30, 20);

//eyes:
  noStroke();
  fill(150, 230, 100);
  ellipse(410, 400, 100, 55);//left outer cicle
  ellipse(590, 400, 100, 55);//right outer cicle
  stroke(153, 210, 102);
  fill(250);
  ellipse(410, 400, 60, 30);//left eyeball
  ellipse(590, 400, 60, 30);//right eyeball
  noStroke();
  fill(80, 55, 45);
  ellipse(410, 400, 18, 24);//left iris
  ellipse(590, 400, 18, 24);//right iris
  fill(0);
  ellipse(410, 400, 9);//left pupil
  ellipse(590, 400, 9); //right pupil
  stroke(148, 238, 98);
  fill(155, 245, 110);
  arc(410, 394, 70, 20, 180, 6, OPEN);//left upper eyelid
  arc(410, 405, 70, 20, 2, 178, OPEN);//left eyelid
  arc(590, 394, 70, 20, 175, -2, OPEN);//right upper eyelid
  arc(590, 405, 70, 20, 2, 178, OPEN);//eyelid

//mouth part 1
  stroke(158, 245, 107);
  fill(149, 236, 98);
  ellipse(500, 560, 43, 75)//philtrum
  stroke(180, 146, 104);
  fill(153, 68, 57)
  rect(323, 560, 354, 140, 65);//mouth open

//tounge
  noStroke();
  fill(162, 53, 38 );
  ellipse(498, 648, 110, 80);
  fill(159, 35, 19);
  ellipse(498, 654, 13, 53);

//teeth
  //upper teeth
  stroke(212, 223, 208);
  fill(238, 229, 218);
  rect(460, 567, 30, 32, 2);//inner teeth
  rect(505, 567, 30, 32, 2);//inner teeth
  fill(230, 220, 207);
  rect(542, 567, 22, 27, 2);//teeth
  rect(430, 567, 22, 27, 2);//teeth
  fill(229, 222, 212);
  rect(573, 567, 14, 25, 2);//teeth
  rect(408, 567, 14, 25, 2);//teeth
  fill(222, 211, 197);
  rect(385, 567, 14, 22, 2);//outer teeth
  rect(598, 567, 14, 22, 2);//outer teeth
  //lower teeth
  stroke(212, 223, 208);
  fill(238, 229, 218);
  rect(456, 647, 30, 32, 2);//inner teeth
  rect(505, 647, 30, 32, 2);//inner teeth
  fill(230, 220, 207);
  rect(554, 649, 22, 27, 2);//teeth
  rect(420, 649, 22, 27, 2);//teeth
  fill(229, 222, 212);
  rect(593, 652, 14, 25, 2);//teeth
  rect(393, 652, 14, 25, 2);//teeth
  fill(222, 211, 197);
  rect(360, 655, 14, 22, 2);//outer teeth
  rect(632, 655, 14, 22, 2);//outer teeth

//mouth part 2
  noStroke();
  fill(180, 146, 104);
  arc(500, 677, 320, 70, 0, 180, OPEN)//underlip
  noStroke();
  fill(180, 146, 104);
  arc(427, 567, 170, 26, 176, -2, CHORD);//left upperlip
  arc(573, 567, 170, 26, 182, 3, CHORD);//right upperlip
  noFill();
  stroke(150, 230, 100);
  curve(236, 450, 303, 565, 421, 484, 746, 600); //left smilewrinkle
  curve(795, 450, 705, 565, 590, 484, 330, 600); //right smilewrinkle

//nose:
  noStroke();
  fill(150, 230, 100);
  ellipse(500, 374, 47, 20);//the form of nose
  ellipse(500, 500, 188, 77);//the form of nose
  quad(478, 370, 522, 370, 595, 500, 405, 500);//the form of nose
  fill(100, 190, 90);
  arc(552, 513, 40, 33, 0, 140, OPEN);//right nostril
  arc(450, 513, 40, 33, 10, 200, OPEN); //left nostril
  stroke(140, 220, 90);
  fill(160, 240, 110);
  ellipse(500, 493, 169, 70); //nosetip
  noStroke();
  ellipse(500, 487, 164, 60); //nosetip
  fill(180, 260, 130);
  ellipse(515, 423, 15, 50);//shadow noseline
  ellipse(530, 505, 40, 14)//shadow nosetip

//eyebrows:
  fill(70, 45, 35);
  angleMode(DEGREES);
  arc(360, 368, 240, 100, 250, 5, CHORD);//left eyebrow
  arc(640, 368, 240, 100, 175, -70, CHORD);//right eyebrow
}

function draw_shrek_happy () {
  strokeWeight(3);
  angleMode(DEGREES)

//the shape of face
  fill(153, 240, 102);
  noStroke();
  ellipse(500, 440, 400, 500);
  triangle(735, 545, 660, 290, 650, 720);
  triangle(272, 549, 340, 290, 350, 720);
  ellipse(505, 592, 480, 349);

//the things on his forehead
  stroke(156, 243, 105);
  fill(150, 230, 100);
  ellipse(400, 280, 40, 30);
  ellipse(450, 240, 30, 20);
  ellipse(500, 260, 40, 30);
  ellipse(590, 260, 40, 30);
  ellipse(565, 225, 30, 20);

//eyes:
  noStroke();
  fill(150, 230, 100);
  ellipse(410, 400, 100, 55);//left outer cicle
  ellipse(590, 400, 100, 55);//right outer cicle
  stroke(153, 210, 102);
  fill(250);
  ellipse(410, 400, 60, 30);//left eyeball
  ellipse(590, 400, 60, 30);//right eyeball
  noStroke();
  fill(80, 55, 45);
  ellipse(410, 400, 18, 24);//left iris
  ellipse(590, 400, 18, 24);//right iris
  fill(0);
  ellipse(410, 400, 9);//left pupil
  ellipse(590, 400, 9); //right pupil
  stroke(148, 238, 98);
  fill(155, 245, 110);
  arc(410, 388, 70, 20, 178, 2, OPEN);//left upper eyelid
  arc(410, 409, 70, 20, 2, 178, OPEN);//left eyelid
  arc(590, 388, 70, 20, 178, 2, OPEN);//right upper eyelid
  arc(590, 409, 70, 20, 2, 178, OPEN);//eyelid

//mouth part 1
  stroke(158, 245, 107);
  fill(149, 236, 98);
  ellipse(500, 560, 43, 75)//philtrum
  stroke(180, 146, 104);
  fill(153, 68, 57)
  arc(502, 565, 360, 150, 0, 180, OPEN)//open mouth

//tounge
  noStroke();
  fill(162, 53, 38 );
  ellipse(498, 600, 110, 80);
  fill(159, 35, 19);
  ellipse(498, 610, 13, 53);

//teeth
  //upper teeth
  stroke(212, 223, 208);
  fill(238, 229, 218);
  rect(460, 567, 30, 32, 2);//inner teeth
  rect(505, 567, 30, 32, 2);//inner teeth
  fill(230, 220, 207);
  rect(542, 567, 22, 27, 2);//teeth
  rect(430, 567, 22, 27, 2);//teeth
  fill(229, 222, 212);
  rect(573, 567, 14, 25, 2);//teeth
  rect(408, 567, 14, 25, 2);//teeth
  fill(222, 211, 197);
  rect(385, 567, 14, 22, 2);//outer teeth
  rect(598, 567, 14, 22, 2);//outer teeth
  //lower teeth
  stroke(210, 221, 206);
  fill(238, 229, 218);
  rect(456, 596, 30, 32, 2);//inner teeth
  rect(505, 596, 30, 32, 2);//inner teeth
  fill(230, 220, 207);
  rect(554, 594, 22, 27, 2);//teeth
  rect(420, 594, 22, 27, 2);//teeth
  fill(229, 222, 212);
  rect(593, 590, 14, 25, 2);//teeth
  rect(393, 590, 14, 25, 2);//teeth
  fill(222, 211, 197);
  rect(360, 586, 14, 22, 2);//outer teeth
  rect(632, 586, 14, 22, 2);//outer teeth

//mouth part 2
  noStroke();
  fill(180, 146, 104);
  arc(502, 609, 290, 62, 0, 180, OPEN)//underlip
  noStroke();
  fill(180, 146, 104);
  arc(417, 568, 195, 35, 180, 0, OPEN);//left upperlip
  arc(588, 568, 195, 35, 180, 0, OPEN);//right upperlip
  noFill();
  stroke(150, 230, 100);
  curve(500, 690, 300, 550, 420, 490, 320, 650); //left smilewrinkle
  curve(500, 690, 710, 550, 580, 490, 580, 650); //right smilewrinkle

//nose:
  noStroke();
  fill(150, 230, 100);
  ellipse(500, 374, 47, 20);//the form of nose
  ellipse(500, 500, 188, 77);//the form of nose
  quad(478, 370, 522, 370, 595, 500, 405, 500);//the form of nose
  fill(100, 190, 90);
  arc(552, 513, 40, 33, 0, 140, OPEN);//right nostril
  arc(450, 513, 40, 33, 10, 200, OPEN); //left nostril
  stroke(140, 220, 90);
  fill(160, 240, 110);
  ellipse(500, 493, 169, 70); //nosetip
  noStroke();
  ellipse(500, 487, 164, 60); //nosetip
  fill(180, 260, 130);
  ellipse(515, 423, 15, 50);//shadow noseline
  ellipse(530, 505, 40, 14)//shadow nosetip

//eyebrows:
  fill(70, 45, 35);
  angleMode(DEGREES);
  arc(400, 348, 160, 90, 180, 0, OPEN);//left eyebrow
  arc(600, 348, 160, 90, 180, 0, OPEN);//right eyebrow
  fill(153, 240, 102);
  arc(401, 353, 160, 40, 180, 0, OPEN);//left eyebrow
  arc(599, 353, 160, 40, 180, 0, OPEN);//right eyebrow
}

function draw_shrek_quirky () {
  strokeWeight(3);
  angleMode(DEGREES)

  //the shape of face
  fill(153, 240, 102);
  noStroke();
  ellipse(500, 440, 400, 500);
  triangle(735, 545, 660, 290, 650, 720);
  triangle(272, 549, 340, 290, 350, 720);
  ellipse(505, 592, 480, 349);

  //the things on his forehead
  stroke(156, 243, 105);
  fill(150, 230, 100);
  ellipse(400, 280, 40, 30);
  ellipse(450, 240, 30, 20);
  ellipse(500, 260, 40, 30);
  ellipse(590, 260, 40, 30);
  ellipse(565, 225, 30, 20);

//eyes:
  noStroke();
  fill(150, 230, 100);
  ellipse(410, 400, 100, 55);//left outer cicle
  ellipse(590, 400, 100, 55);//right outer cicle
  stroke(153, 210, 102);
  fill(250);
  ellipse(410, 400, 60, 30);//left eyeball
  ellipse(590, 400, 60, 30);//right eyeball
  noStroke();
  fill(80, 55, 45);
  ellipse(410, 400, 18, 24);//left iris
  ellipse(590, 400, 18, 24);//right iris
  fill(0);
  ellipse(410, 400, 9);//left pupil
  ellipse(590, 400, 9); //right pupil
  stroke(148, 238, 98);
  fill(155, 245, 110);
  arc(410, 403, 70, 36, 178, 2, OPEN);//left upper eyelid
  arc(410, 406, 70, 30, 2, 178, OPEN);//left eyelid
  arc(590, 388, 70, 20, 178, 2, OPEN);//right upper eyelid
  arc(590, 408, 70, 20, 2, 178, OPEN);//eyelid

//mouth part 1
  stroke(158, 245, 107);
  fill(149, 236, 98);
  ellipse(500, 560, 43, 75)//philtrum
  stroke(180, 146, 104);
  fill(153, 68, 57)
  arc(502, 573, 360, 132, -6, 168, OPEN)//open mouth

//tounge
  noStroke();
  fill(162, 53, 38 );
  ellipse(498, 600, 70, 40);
  fill(159, 35, 19);
  ellipse(498, 610, 3, 43);

//teeth
  //upper teeth
  stroke(212, 223, 208);
  fill(222, 211, 197);
  rect(371, 595, 9, 17, 2);//outer teeth
  rect(642, 566, 9, 17, 2);//outer teeth
  fill(229, 222, 212);
  rect(408, 589, 9, 20, 2);//teeth
  rect(618, 570, 9, 20, 2);//teeth
  fill(230, 220, 207);
  rect(432, 590, 17, 22, 2);//teeth
  rect(578, 573, 17, 22, 2);//teeth
  fill(238, 229, 218);
  rect(466, 580, 25, 27, 2);//inner teeth
  rect(519, 580, 25, 27, 2);//inner teeth
  //lower teeth
  stroke(210, 221, 206);
  fill(222, 211, 197);
  rect(384, 600, 14, 22, 2);//outer teeth
  rect(654, 580, 14, 22, 2);//outer teeth
  fill(229, 222, 212);
  rect(422, 605, 14, 25, 2);//teeth
  rect(631, 590, 14, 25, 2);//teeth
  fill(230, 220, 207);
  rect(460, 608, 22, 27, 2);//teeth
  rect(600, 594, 22, 27, 2);//teeth
  fill(238, 229, 218);
  rect(502, 600, 30, 32, 2);//inner teeth
  rect(557, 596, 30, 32, 2);//inner teeth

//mouth part 2
  push();
  strokeWeight(8);
  stroke(180, 146, 104);
  noFill();
  arc(502, 573, 360, 132, -6, 168, OPEN)//open mouth
  pop();
  noStroke();
  fill(180, 146, 104);
  arc(511, 611, 260, 58, 2, 155, OPEN)//underlip
  push();
  strokeWeight(10);
  stroke(180, 146, 104);
  noFill();
  beginShape(); //line in the mouth
  curveVertex(345, 602);
  curveVertex(345, 602);
  curveVertex(466, 584);
  curveVertex(500, 585);
  curveVertex(534, 575);
  curveVertex(672, 557);
  curveVertex(672, 557);
  endShape();
  pop();
  noFill();
  stroke(150, 230, 100);
  curve(241,460,328,575,426,494,751,600); //left smilewrinkle
  curve(795,460,730,575,590,494,330,600); //right smilewrinkle

//nose:
  noStroke();
  fill(150, 230, 100);
  ellipse(500, 374, 47, 20);//the form of nose
  ellipse(500, 500, 188, 77);//the form of nose
  quad(478, 370, 522, 370, 595, 500, 405, 500);//the form of nose
  fill(100, 190, 90);
  arc(552, 513, 40, 33, 0, 140, OPEN);//right nostril
  arc(450, 513, 40, 33, 10, 200, OPEN); //left nostril
  stroke(140, 220, 90);
  fill(160, 240, 110);
  ellipse(500, 493, 169, 70); //nosetip
  noStroke();
  ellipse(500, 487, 164, 60); //nosetip
  fill(180, 260, 130);
  ellipse(515, 423, 15, 50);//shadow noseline
  ellipse(530, 505, 40, 14)//shadow nosetip

//eyebrows:
  fill(70, 45, 35);
  angleMode(DEGREES);
  arc(397, 371, 180, 45, 185, 8, OPEN);//left eyebrow
  arc(600, 353, 160, 90, 180, 0, OPEN);//right eyebrow
  fill(153, 240, 102);
  arc(599, 355, 160, 40, 180, 0, OPEN);//right eyebrow
}
