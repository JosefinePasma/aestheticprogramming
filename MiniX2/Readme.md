### Los Emoji Project  
##### MiniX2
  [Click here for my emoji](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX2/)

  [Click here for the code](https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX2/sketch.js)

##### Picture of the Emoji Project
![](OGRES_ARE_LIKE_ONIONS.png)


##### Describe your program and what you have used and learnt.
My program is an emoji based on the face of the character Shrek from Shrek. The program can by a click on different buttons shift facial expressions based on the emotions normal, happy, quirky, and angry. A program inspired by the way [Erna Holst Rokne](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix2/README.md) did her MiniX2, where you by a click on buttons can shift between facial expressions which draw my attention and made me make something similar.The emoji of Shrek is constructed from different shapes and colors. Between a lot of different shapes, I used to make Shrek are ellipse, arc, and rect some of the most commonly used shapes. Besides playing a lot with shapes I also played with colors. Colors is excellent because you can make a face more realistic by adding new shapes such as an arc or an ellipse and with just a bit of tone shift from the original face color, you then have wrinkles or/and shadows. Besides the shape and color, I also had a lot of help with; noStroke(), strokeWeight(), stroke(), fill(), noFill(). I had a lot of fun with this project and making the different expressions of Shrek, getting to learn more of the importance of order and how to make what I want.  

##### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?
Emojis are a frequently used expression mediator, a thing I think most people use frequently in their everyday lives. But, as expressed in the chapter "Variable Geometry" by Soon and Cox, emojis do also come with "underlying issues related to the politics of representation"(2020). In this text, they mention some of these representation issues and among these, they mention racial discrimination like limitations of skin tone applied to emojis. This is where Shrek comes in. Instead of using the human face, you could use Shrek’s face. If this was done I think no racial discrimination in terms of limitations of skin tone applied would appear because Shrek is green and no one else is green, thereby no one will be offended that their skin tone is not represented. But there may need to be a Fiona too. Besides that I think no one would be offended to receive a text with an expression by Shrek - everybody loves Shrek, he is awesome, fun and cool.  

"Ogres is like onions. Onions have layers, ogres have layers."



###### References
>Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70

>https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix2/README.md
