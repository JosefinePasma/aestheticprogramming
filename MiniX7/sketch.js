let screen = 0;
let score= 0;
let baby = [];
let min_baby = 5;
let persSize = {
  w: 150,
  h: 150
}
let x =200;
let persPosX;
let babies = [];

function preload(){
  bg = loadImage("Shower.png");
  baby1 = loadImage("baby1.png");
  baby2 = loadImage("baby2.png");
  baby3 = loadImage("baby3.png");
  persons = loadImage("persons.png");
  bloodS = loadImage("bloodSplatter.png");
  babies = [baby1, baby2, baby3];
}

function setup() {
  createCanvas(1200, 700);
  textAlign(CENTER);
  persPosX = 600
}

function draw() {
	if(screen == 0){
    startScreen();
  }else if(screen == 1){
  	gameOn();
  }else if(screen==2){
  	endScreen();
  }
}

function startScreen(){
	background(254, 255, 171);
	fill(255, 31, 53);
  textSize(40);
	textAlign(CENTER);
	text('WELCOME TO YOUR BABYSHOWER', width/2, height/2);
	text('Click to start the party!', width/2, height/2+50);
  reset();
}

function gameOn(){
  background(bg);
  fill(255, 31, 53);
  textSize(25);
  text("score = "+ score, width/2, 690);
  textSize(15);
  text("Move the cursor to catch your babies", width/2, 670);
  textSize(40);
  text("BABYSHOWER", width/2, 130);
  push();
  imageMode(CENTER);
  image(persons, mouseX, height-120, persSize.w, persSize.h);
  pop();
  checkBabyNum();
  showBaby();
  checkCatching();
}

function checkBabyNum() {
  if (baby.length < min_baby) {
    baby.push(new Baby());
  }
}

function showBaby(){
  for (let i = 0; i <baby.length; i++) {
    baby[i].move();
    baby[i].show();
  }
}

function checkCatching(){
  for (let i = 0; i < baby.length; i++) {
    let d =
      dist(mouseX, 500,baby[i].x, baby[i].y);
    if(baby[i].y > height){
      screen = 2
    }
    if(d < persSize.h/2){
      score+= 1
      baby.splice(i,1)
    }
  }
}

function endScreen(){
	background(254, 255, 171);
  image(baby1, 90, 150, 300, 400);
  image(baby2, 410, 90, 300, 400);
  image(baby3, 660, 350, 300, 300);
  image(bloodS, width/8, height/8, 800,600);
  fill(255);
  textSize(30);
  text('BABY is DEAD', width / 2, height / 2 -10);
  textSize(20);
  text("SCORE = " + score, width / 2, height / 2 + 20);
  textSize(15);
  text('Click to play again', width / 2, height / 2 + 60);
}

function mousePressed(){
	if(screen==0){
  	screen=1
  }else if(screen==2){
    baby.splice(0,baby.length)
  	screen=0
  }
}

function reset(){
  score=0;

}
