### Revisit the past
#### MiniX7
![](BabyGame2.0.png)
  [Click here for the program](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX7/),  [click here for the sketch code](https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX7/sketch.js) and [click here for the babies code.](https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX7/baby.js)


#### Description of the revisit
For this week we had to revisit the past, which meant that we had to look back on our old projects and choose one of these to explore further and design iteratively. I actually did not go that far back in my past - I chose to revisit my MiniX6. I chose to rework my MiniX6 because I wanted to achieve a better understanding of object-oriented programming and besides this, I was also not fully satisfied with the outcome of my MiniX6.

The overall idea is still the same; 'Catch the babies with the cursor otherwise they will die'. It is the things I was not satisfied with in the game I have changed. I was not satisfied with my outcome of the game, because the game first of all was way to easy. To change this I added more babies in my min_baby from 2 to 5, furthermore I added a random speed for the babies so that the speed now is random(3,6) instead of 3. The other reason I was not fully satisfied with the game was because I really wanted the babies who is falling to be random between the individual baby picture and not all three of the baby picture falling at the same time. To change this I made an array called babies, then I called each of the baby pictures in this array, under constructor of these baby-objects I then made a this.randoImg where random(babies) is called and finally putting this.randoImg into the image.

What I have learnt in this miniX is the wanted better understanding of object-oriented programming. I understand why it makes sense to make a Class and use this class throughout the sketch instead of calling it yourself. I have now also understand the Class in itself - meaning all these if statements and how they function, including in particular how I could make the image/baby random, since this is what I had a really hard time getting to work in my miniX6.

#### Aesthetic Programming
The relation between aesthetic programming and digital culture, is that aesthetic programming allows a new kind of critical thinking of the digital culture within the wider social, cultural and political implications of programming and of the aesthetics of software. Soon and Cox notes it in the preface of their book, Aesthetic Programming, with the quote; “(…)is a means to engage with programming to question existing technological paradigms and further create changes in the technical system.”(2020).  Aesthetic programming is thereby a tool to think and act on the digital culture. Not only to learn how to do programming, but to think of what the relations are, what implications will happen and what affects does it have to our world.


When creating the code and creating codes in generally throughout this course I have learnt to think about the choices I make and look at the other perspectives. For this work in now both MiniX6 and MiniX7, I really tried to not be bias or unintentionally exclude someone, tried not to be be racist or sexists by including different babies and calling those who is catching the babies persons and not parents. But I think that even with these choices I have made, someone will be offended by my program, because I have included something and thereby excluded something else. So even though I think a lot about how to be political correct, it is not that perfect -  to why Aesthetic Programming is important, because hereby the problem can be addressed and a change can happen.  

###### References
* Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 13-24
