class Baby {
  constructor() {
    this.x= random(20,width-50)
    this.y = -20;
    this.size = floor(random(40,100));
    this.speed = floor(random(3,6));
    this.randoImg = random(babies)
  }
  move (){
    this.y += this.speed;
  }

  show() {
    push();
    image(this.randoImg, this.x, this.y, this.size, this.size);
    pop();
  }
}
