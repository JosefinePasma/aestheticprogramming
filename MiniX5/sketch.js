
h = [5, 16, 27, 38, 49, 60, 71, 82, 93, 104, 115, 126, 137, 148, 159, 170, 181, 192, 203, 214, 225, 236, 247, 258, 269, 280, 291, 302, 313, 324, 335, 346, 357, 368, 379, 390, 401, 412, 423, 434, 445, 456, 467, 478, 489, 500, 511, 522, 533, 544, 555, 566, 577, 588, 599, 610, 621, 632, 643, 654, 665, 676, 687, 698, 709, 720, 731, 742, 753, 764, 775, 786, 797, 808, 819, 830, 841, 852, 863, 874, 885, 896, 907, 918, 929, 940, 951, 962, 973, 984, 995, 1006, 1017,1028,1039,1050,1061,1072,1083,1094]

function setup () {
  createCanvas(1100, 803)
  background(0)
  for(let i = 0; i < 1100; i++) {
    rect(0+i, 0, 10, 10)
      i += 10
    for (let t = 0; t < 803; t++) {
      rect(-10+i, 11+t, 10, 10)
      t += 10
      }
    }
}

function draw () {
  if ( random(1) < 0.5){
    randomRect()
  } else {
    whiteRect()
  }
}

function randomRect() {
  push()
  noStroke()
  rectMode(CENTER)
  fill(random(255), random(255), random(255))
  rect(random(h), random(h), 9, 9)
  pop()
}

function whiteRect () {
  push()
  noStroke()
  fill(255)
  rectMode(CENTER)
  rect(random(h), random(h), 9, 9)
  pop()
}
