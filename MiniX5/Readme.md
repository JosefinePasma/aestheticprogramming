### Auto-Generator
#### MiniX5
So to start with I just want to let whoever reading this know I actually did think this was a very exciting and interesting task to do. This week though, I had no success with creativity or make what I actually wanted to do work (when I got an idea) - very frustrating and used a lot of time to try.

So this is what I ended up with:
![](pixelSomething.png)
  [Click here for the program](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX5/) and   [click here for the code.](https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX5/sketch.js)


#### Description of the program
So this program is quite simple and the rules are therefore also quite simple
1. create a lot of different colored rects
2. create white rects so there will be places where you might can see something from the other rects.

The program performs by starting with a plain grid, where there then is being drawn rects in different colors on this grid. Gradually there are more and more rects with color being drawn and some kind of picture (depending on the users fantasy or interpretation) is being shown. For the outcome to be not only rects in different colors, there is also white rects being drawn.

The rules produce emergent behavior in no way in my program. There is nothing that makes the wished "simple rules - complex patterns" in my program. I think this is because of the fact that we needed to have 2 rules, and also needed to have a for/while-loop along with a conditional statement. I found it hard to incorporate all we needed in a good context for the program. So one become weaker - being the rules. In a very broad search for it though, one can say that the simple rects to be drawn are constantly being drawn and in that way together create an image. So the rects operate together to create a more complex picture.

#### Role of the rules
As said before, I had a very difficult time being creative and getting what I actually wanted the program to do work. Therefore the role of the rules is not important.

The role of process is slightly more important, because something will be created over time. With no process of it, there will not really be created a pixel-picture.


#### "Auto-generator"
I found the topic with auto-generator very interesting. The fact that upon only giving the program some rules it creates some kind of "random" picture and you do not really now what the outcome will be because it is the program that performs the rules, as LeWitt very well explains it; “The idea becomes a machine that makes the art”(Soon & Cox). I understand that the outcome not actually is that random as it seems, but the control is not only in your own hands - because you actually just give it some rules to what it has to follow, but at first glance you can be surprised on how the program follows/reacts to these rules.

.
.
.

Low floors - high cellings ;)

###### References
* Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
* Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
