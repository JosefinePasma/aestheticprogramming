### The Revealing of Digital Surveillance  
#### MiniX4
'The Revealing of Digital Surveillance' is a project meant to address the thoughts on the need to capture data on everything, as expressed in the book Aesthetic Programming (Soon & Cox): “…that in the era of big data, there appears to be a need to capture data on everything, even from the most mundane actions like button pressing.”. In this project every time you click the button, it is revealed that you gave the program some data, which is now used to reveal  you as a kind of Digital Surveillance. When you think you can just stop it with a new click on a button, it actually just get worse and they achieve more data from you. The more data you give – the more you are being revealed.   

![](Surveillance.png)
  [Click here for the program](https://josefinepasma.gitlab.io/aestheticprogramming/MiniX4/) and   [click here for the code.](https://gitlab.com/JosefinePasma/aestheticprogramming/-/blob/main/MiniX4/sketch.js)


#### Description of the program
It is a program using both the webcam, audio and button/mouse to capture the users data. The program starts by having an all blue screen with a window asking if the user want to accept cookies and if not, you have to close that window. If this is done what happens is that the blue background becomes weaker and it is revealed that the user is being ‘watch’. Along with this a new window pops up telling the user that if you want to stop it you just have to close that window. What actually happens is a long process of the background getting weaker as long as the user keeps closing the windows that pops up and those ‘watching’ is only capturing more data of the user. By the end all the user has to do is scream and it will stop with a picture of the user screaming. To make the program I have used and learnt syntax such as createButton, createCapture and new p5.AudioIn, along with if statements. Every time the alpha value ‘blue’ goes down by 20 a new if statement is true and a new ‘window’ is being called with a new button to press and new text being displayed. To make the window go around in different placements I also had a lot of help with translate().

##### "Capture all"
I would say that my work do relate to the theme of “capture all”. This is because with each time the user clicks on the button ‘some data’ about the user is captured, but this ‘some data’ is not only the click on the button it is also the user in live video – what that person is doing right now. Each time the user gives more data a click on the button, more data about the user in live video is also achieved – only getting worse by each new click. The user gives data as a “surveillance capitalism” which is addressed in article Datafication(Mejias): ” Shoshana Zuboff argues that what we are living through is a new stage of “surveillance capitalism” (2019) in which human experience becomes the raw material that produces the behavioural data used to influence and even predict our actions.”. They watch and capture every thing you do. 

###### References
* Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

* Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. https://policyreview.info/concepts/datafication
