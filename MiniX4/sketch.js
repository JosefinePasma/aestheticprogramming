let capture;
let blue = 255;
let button;
let mic;
let treshold = 0.1

let t = ["DO YOU WANT TO ACCEPT COOKIES", "OH SOMEONE IS KEEPING AN EYE ON YOU", "STOP THE VIDEO RECORDING", "LOL - DID NOT WORK", "GODDAMN YOU ARE LOOKING GOOD", "YOU ARE ONLY MAKING IT WORSE", "CLICK, CLICK, CLICK - IT IS NOT WORKING","VIDEO, VIDEO - VIEDO!", "F*** IT IS STILL RECORDING", "THE VIDEO RECORDING IS STILL ON", "GIVE ME, GIVE ME, GIVE ME", "ARHHhhh", "IF YOU WANT TO STOP THE VIDEO RECORDING"]
let u = ["Close this window if not", "Close the window if you want to stop it", "Close the window to stop it","Close the window to stop it and it will work now", "Can still see you - close the window now", "Close the window ", "Try to close the window now", "Den køre nat og dag - if you do not close the window now","What happens if you try to close the window now?", "Maybe it will stop after you close this window ", "Just one more click", "One more time - click the window to stop video", "Just scream"]

let index = 0;

function setup() {
  createCanvas(640,480);
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();

  button = createButton("x")
  button.style("background", "#f22727")
  button.style("color", "#ffffff")
  button.size(20,20)
  button.position(300, 360)
  button.mousePressed(change)

  mic = new p5.AudioIn();
  mic.start();
}

function draw() {
  image(capture, 0,0, 640, 480);
  fill(0, 0, 155, blue);
  rect(0,0,640,480);
  let vol = mic.getLevel();
  if (blue == 255){
    translate(218, 190)
    fane()
    button.position(402, 190)
  }
  if (blue == 235){
    translate(50, 100)
    fane()
    button.position(234, 100)
  }
  if(blue == 215){
    translate(60, 250)
    fane()
    button.position(244, 250)
  }
  if(blue == 195){
    translate(200, 350)
    fane()
    button.position(384, 350)
  }
  if(blue == 175){
    translate(400, 70)
    fane()
    button.position(584, 70)
  }
  if(blue == 155){
    translate(320, 50)
    fane()
    button.position(504, 50)
  }
  if(blue == 135){
    translate(320, 240)
    fane()
    button.position(504, 240)
  }
  if(blue == 115){
    translate(360, 300)
    fane()
    button.position(544, 300)
  }
  if(blue == 95){
    translate(50, 200)
    fane()
    button.position(234, 200)
  }
  if(blue == 75){
    translate(420, 370)
    fane()
    button.position(604, 370)
  }
  if(blue == 55){
    translate(50, 200)
    fane()
    button.position(234, 200)
  }
  if(blue == 35){
    translate(430, 100)
    fane()
    button.position(614, 100)
    userStartAudio ();
  }
  if(blue == 15){
    translate(245, 200)
    fane()
  }
  if(vol > treshold) {
    capture.stop();
  }
}

function change () {
  button.position(-100, -100)
  blue -= 20;
  index = index + 1;
}

function fane () {
  noStroke ();
  fill(170)
  rect(0, 0, 204, 100)
  stroke(0);
  line(0, 21, 204, 21)
  fill(0)
  textSize(8)
  textAlign(CENTER);
  text(t[index],102, 45)
  text(u[index],102, 70)
}
