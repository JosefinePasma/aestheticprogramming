let data;
let world; //Refer to our data with "First World Problems"
let u; //Refer to data with statements about the situation in Ukraine
let blink = 0

function preload() {
  data = loadJSON("problems.json") //Loading the JSON file
  b = loadImage("u.png") //Background picture
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(1)
}

function draw() {
  background(b);
  world= data.WorldP; //WorldP is our data from JSON
  u = data.Ukraine; //Ukraine is our data from JSON

  let i = floor(random(0,21)) //Picks a random statement from the array with "First World problems"
  let j = floor(random(0,16)) //Picks a random statement from the array with Ukraine

  let statement = world[i] //Used to put the text in. World[i] refer to world arrayet witch is random between 0 - 21
  let statement2 = u[j] //Used to put the text in. U[i] refer to U arrayet witch is random between 0 - 16

// All the boxes
  noStroke();
  fill(255,180);
  rect(80,35,1800,800);
  fill(0,180);
  rect(110,133,500,30);
  rect(110,433,500,30);

//All the text
  push();
  fill(255,0,0);
  textFont("Courier New");
  textSize(40);
  textStyle(BOLD);
  text("It's horrible when...", 110,150);
  text(statement, 390, 230)
  text("But at least you...", 110,450);
  text(statement2, 390, 530 );
  pop();

  push();
  blink += 1;
  if(blink % 6 <= 4){ //after the number is divided by 6 there might be a remainder, if this remainder is smaller than or equal to 4 the last text is red and if not there is no text
  fill(255,0,0);
  textFont("Courier New");
  textSize(50);
  textStyle(BOLD);
  text("... YOU ARE NOT IN WAR.", 150,750);
  pop();
  } else {
  fill(255,0);
  textFont("Courier New");
  textSize(50);
  textStyle(BOLD);
  text("... YOU ARE NOT IN WAR.", 150,750);
  pop();
  }
}
